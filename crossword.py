"""
WWWWWWWWBWWWWWW
WBWBWBWBBBWBWBW
WWWWWWWWBWWWWWW
WBWBWBWBWBWBWBW
WWWWWWWWWWWBBBB
WBWBBBWBWBWBWBW
WWWWWBWWWWWWWWW
WBWBWBWBWBWBWBW
WWWWWWWWWBWWWWW
WBWBWBWBWBBBWBW
BBBBWWWWWWWWWWW
WBWBWBWBWBWBWBW
WWWWWWBWWWWWWWW
WBWBWBBBWBWBWBW
WWWWWWBWWWWWWWW
 
(0,0,1)
(0,3,2)
(0,4,3)
(0,6,4)
(0,9,5)
(0,10,6)
(0,12,7)
(0,14,8)
"""
def parse_input(input_string) ->list[int]:
    rows = input_string.strip().split()
    grid = [list(row) for row in rows]
    return grid

def is_start_of_word_across(grid, row, col) -> bool:
    return (col == 0 or grid[row][col - 1] == 'B') and (col < len(grid[row]) - 1 and grid[row][col + 1] == 'W')

def is_start_of_word_down(grid, row, col) -> bool:
    return (row == 0 or grid[row - 1][col] == 'B') and (row < len(grid) - 1 and grid[row + 1][col] == 'W')

def number_crossword(grid) -> list[int]:
    number = 1
    numbered_positions = []
    rows, cols = len(grid), len(grid[0])

    for row in range(rows):
        for col in range(cols):
            if grid[row][col] == 'W':
                if is_start_of_word_across(grid, row, col) or is_start_of_word_down(grid, row, col):
                    numbered_positions.append((row, col, number))
                    number += 1

    return numbered_positions

def give_numbered_positions(numbered_positions):
    for pos in numbered_positions:
        return pos

def main(input_string):
    grid = parse_input(input_string)
    numbered_positions = number_crossword(grid)
    give_numbered_positions(numbered_positions)



input_string = (
        "WWWWWWWWBWWWWWW "
        "WBWBWBWBBBWBWBW "
        "WWWWWWWWBWWWWWW "
        "WBWBWBWBWBWBWBW "
        "WWWWWWWWWWWBBBB "
        "WBWBBBWBWBWBWBW "
        "WWWWWBWWWWWWWWW "
        "WBWBWBWBWBWBWBW "
        "WWWWWWWWWBWWWWW "
        "WBWBWBWBWBBBWBW "
        "BBBBWWWWWWWWWWW "
        "WBWBWBWBWBWBWBW "
        "WWWWWWBWWWWWWWW "
        "WBWBWBBBWBWBWBW "
        "WWWWWWBWWWWWWWW"
    )
grid = parse_input(input_string)
result = number_crossword(grid)
print(result) 

